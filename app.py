from flask import Flask, redirect, url_for, jsonify, session, request, render_template,g
from flask_dance.contrib.google import google
from routes.auth import auth, blueprint, admin_required
from routes.services import services
from routes.admin_panel import admin_panel

app = Flask(__name__)
app.secret_key = "aled"
app.register_blueprint(auth)
app.register_blueprint(blueprint, url_prefix="/login")
app.register_blueprint(services, url_prefix='/services')
app.register_blueprint(admin_panel, url_prefix='/admin')

@app.route("/")
def index():
    return render_template("index.html", connected=google.authorized, pp=session.get("profile_img_url","no_image"))

@app.route("/profile")
def profile():
    if not google.authorized:
        return redirect(url_for("google.login"))

    resp = google.get("/oauth2/v2/userinfo")
    if not resp.ok:
        return jsonify({"error": "Failed to fetch user info from Google."}), 400

    return jsonify(resp.json())


@app.route('/about')
def about():
    return render_template("about.html", connected=google.authorized, pp=session.get("profile_img_url","no_image"))



# DB PART


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


if __name__ == "__main__":
    #with app.app_context():
    #    init_debug_db()
    app.run()

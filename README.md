# Logistique CETEN

Ce site contiendra un système d'automatisation et de digitalisation des formulaires de logistique du BDE

## Pour lancer

```sh
docker build -t log_ceten
docker run -p 5000:5000 log_ceten
```
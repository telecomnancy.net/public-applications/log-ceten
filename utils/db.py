from flask import g
import sqlite3

DATABASE = "resources.db"

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
    return db

def reset_db():
    statements = ["DROP TABLE IF EXISTS items",
                  "DROP TABLE IF EXISTS places",
                  "DROP TABLE IF EXISTS loans",
                  "CREATE TABLE items (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, place_id INTEGER, taken BOOLEAN, taken_by TEXT)",
                  "CREATE TABLE places (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT)",
                  "CREATE TABLE loans (id INTEGER PRIMARY KEY AUTOINCREMENT, email_loaner TEXT, id_item INTEGER, begin_date DATETIME, end_date DATETIME, under TEXT, caution_status INTEGER ,pending_approval BOOLEAN)"]
    # caution_status : 0 = caution ceten, 1 = caution custom, 2 = si caution custom, en attente de caution (donc emprunt accepté), 3 = cautio nreçue
    db = get_db()
    for s in statements:
        db.execute(s)
    db.commit()

def init_debug_db():

    reset_db()

    statements = ["INSERT INTO places (name) VALUES ('Douche')",
                  "INSERT INTO places (name) VALUES ('R5')",
                  "INSERT INTO places (name) VALUES ('Local BDE')",
                  "INSERT INTO places (name) VALUES ('Local Club Animest')",
                  "INSERT INTO items (name, place_id, taken) VALUES ('Appareil à raclette',1,0)",
                  "INSERT INTO items (name, place_id, taken) VALUES ('Appareil à raclette',1,0)",
                  "INSERT INTO items (name, place_id, taken) VALUES ('Appareil à raclette',1,1)",
                  "INSERT INTO items (name, place_id, taken) VALUES ('Appareil à raclette',2,0)",
                  "INSERT INTO items (name, place_id, taken) VALUES ('Ordinateur',2,1)"
    ]

    db = get_db()
    for s in statements:
        db.execute(s)
    db.commit()


def add_loan(email_loaner, id_item, begin_date, end_date, under, caution_status):
    db = get_db()
    db.execute("INSERT INTO loans (email_loaner, id_item, begin_date, end_date, under, caution_status, pending_approval) VALUES (?,?,?,?,?,?,?)", [email_loaner, id_item, begin_date, end_date, under, caution_status,1])
    db.commit()

def get_pending_loans():
    db = get_db()
    cur = db.execute("SELECT * FROM loans WHERE pending_approval = 1")
    return cur.fetchall()

def get_ongoing_loans():
    db = get_db()
    cur = db.execute("SELECT * FROM loans WHERE pending_approval = 0 AND end_date > CURRENT_DATE")
    return cur.fetchall()

def get_late_loans():
    db = get_db()
    cur = db.execute("SELECT * FROM loans WHERE pending_approval = 0 AND end_date <= CURRENT_DATE")
    return cur.fetchall()

def get_items_dict():
    # Will return a dict whose keys are ids, and values or the rest
    db = get_db()
    cur = db.execute("SELECT * FROM items")
    data = cur.fetchall()
    res = {}
    for row in data:
        res[row[0]] = {}
        res[row[0]]["name"] = row[1]
        res[row[0]]["place_id"]= row[2]
        res[row[0]]["taken"] = row[3]
        res[row[0]]["taken_by"] = row[4]

    return res

def accept_loan(loan_id):
    db = get_db()
    db.execute("UPDATE loans SET pending_approval = 0 WHERE id = ?", [loan_id])
    db.execute("UPDATE loans SET caution_status = 2 WHERE id = ? AND caution_status = 1", [loan_id])
    db.commit()

def delete_loan(loan_id):
    db = get_db()
    db.execute("DELETE FROM loans WHERE id = ?", [loan_id])
    db.commit()


def caution_given(loan_id):
    db = get_db()
    db.execute("UPDATE loans SET caution_status = 3 WHERE id = ? AND caution_status = 2", [loan_id])
    db.commit()

def get_places():
    db = get_db()
    cur = db.execute("SELECT * FROM places")
    data = cur.fetchall()
    res = {}
    for row in data:
        res[row[0]] = {}
        res[row[0]]["name"] = row[1]

    return res
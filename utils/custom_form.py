class CustomForm:
    def __init__(self, form_dict, custom_args):
        """
            form_dict is a variable that follows a json syntax :
            {
                "action" : "endpoint",
                fields : {
                    "Name of field 1" : {
                        "answer_type" : ..., // Could be 'drop_down', 'text', 'checkboxes', 'radio'
                        "place_holder" : ... // Could be None or '$a_key_of_custom_args$' and it will take the value
                    }, ...
                }
            }
        """

        self.form_dict = form_dict
        self.custom_args = custom_args

        print(f'{custom_args=}')

    def standardize(self, text):
        return text.replace(" ", "_").lower()

    def to_html(self):
        buff = f"<form action='{self.form_dict['action']} method='post'>\n"

        section_amount = len(set([f["fields"][n] for n in []]))

        sections = []
        for field_name, field_args in self.form_dict["fields"]:
            std_f_name = self.standardize(field_name)
            field_type = field_args['answer_type']

            buff += f"<label for={std_f_name}>{field_name} : </label>\n"
            if field_type == "text":
                pass

        buff += f"<input type='submit' value='{self.form_dict['submit_text']}'>\n"
        buff += '</form>'
        return buff

if __name__ == "__main__":
    t = CustomForm({
        "action" : "/test"
        "submit_text":"envoyer !"
        "fields" : {
            "test" : {
                "section" : 1
                "answer_type" : 'text',
                "place_holder" : None
            },
            "pouet" : {
                "section" : 1
                "answer_type" : 'drop_down',
                "place_holder" : '$salut'
            },
            "pouet pouet" : {
                "section" : 2
                "answer_type" : 'drop_down',
                "place_holder" : '$salut'
            }
        }
    }, { 'salut' : ['one','two','three','viva algerie']})
    print(t.to_html())

    
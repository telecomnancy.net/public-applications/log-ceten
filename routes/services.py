from flask import Blueprint, session, redirect, url_for, flash, render_template, request, current_app
from flask_dance.contrib.google import google
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, SelectField, DateTimeLocalField, RadioField, BooleanField
from wtforms.validators import DataRequired, Length
from utils.db import add_loan, get_items_dict

services = Blueprint('services', __name__)

class EmpruntBDEForm(FlaskForm):
    # Todo : validators
    #item_select = SelectField('Item', choices=[("-1", "Nouvel Item")], )
    item_select = SelectField('Item', choices=[])
    sous = StringField('Sous : BDE/CLUB')
    #custom_item_select_name = StringField('Nouveau nom d\'item',  )
    #custom_item_select_place= StringField('Nouveau nom d\'item',  )
    date_debut = DateTimeLocalField('Date du début de l\'emprunt',   )
    date_fin = DateTimeLocalField('Date de la fin de l\'emprunt',   )
    caution = RadioField("La caution est", choices=[(0,"le chèque de cuation donné lors de l'inscription"),(1,"un chèque à donner au BDE")])
    accept = BooleanField("J'accepte les conditions décrites plus haut et que le CETEN se réserve le choix du montant en cas de casse de l'appareil, en fonction des dommages engendrés")
    submit = SubmitField('Envoyer la demande', render_kw={'type':'submit'})

@services.route("/emprunt_bde", methods=["POST","GET"])
def emprunt_bde():
    if not google.authorized:
        return redirect(url_for("google.login"))
    
    form = EmpruntBDEForm()
    form.item_select.choices = form.item_select.choices + [(id, o['name']) for id, o in get_items_dict().items()]

    if form.validate_on_submit():
        with current_app.app_context():
            add_loan(session['email'], form.item_select.data,form.date_debut.data, form.date_fin.data,form.sous.data, form.caution.data)
        flash('Demande faite!', 'success')
        return redirect(url_for('services.emprunt_bde')) 
    else:
        print(form.errors)
    return render_template("emprunt_bde.html", connected=google.authorized, pp=session.get("profile_img_url","no_image"), form=form)

@services.route('/index')
def index():
    if not google.authorized:
        return redirect(url_for("google.login"))
    
    return render_template("services.html", connected=google.authorized, pp=session.get("profile_img_url","no_image"))

from flask import Blueprint, session, redirect, url_for, flash
from flask_dance.contrib.google import make_google_blueprint, google
from functools import wraps
from flask_dance.consumer import oauth_authorized
from dotenv import load_dotenv
load_dotenv()

import os
ADMIN_USERS = ["lotcelan@gmail.com"]


auth = Blueprint('auth', __name__)

client_id = os.environ.get("GOOGLE_CLIENT_ID")
client_secret = os.environ.get("GOOGLE_CLIENT_SECRET")

blueprint = make_google_blueprint(
    client_id=client_id,
    client_secret=client_secret,
    scope=["profile", "email"],

)

def admin_required(f):

    @wraps(f)
    def decorated_function(*args, **kwargs):
        if session.get('is_admin') is not True:
            flash("You are not admin !", category="error")
            return redirect(url_for('index'))
        return f(*args, **kwargs)
    return decorated_function

@auth.route("/authenticate")
def authenticate():
    return redirect(url_for("google.login"))

"""
@auth.route("/")
def index():
    if not google.authorized:
        return redirect(url_for("google.login"))
    resp = google.get("/oauth2/v2/userinfo")
    assert resp.ok, resp.text
    return f"Welcome, {resp.json()['name']}!"
"""

@auth.route("/logout")
def logout():
    token = blueprint.token  # Get the current token

    if token is not None:
        # Delete the token
        del blueprint.token

    # You can also use Flask's session to remove user details if you have stored any
    session.clear()

    return redirect(url_for("index"))

@oauth_authorized.connect_via(blueprint)
def google_logged_in(blueprint, token):
    if not token:
        flash("Failed to log in with Google.", category="error")
        return False

    resp = blueprint.session.get("/oauth2/v2/userinfo")
    if not resp.ok:
        msg = "Failed to fetch user info from Google."
        flash(msg, category="error")
        return False


    info = resp.json()
    print(info)
    flash(f"Welcome {info['name']}")
    
    session["profile_img_url"] = info["picture"]
    session["email"] = info["email"]
    # Check if the user is an admin

    print(f"{info['email']=}")
    if info["email"] in ADMIN_USERS:
        session['is_admin'] = True
    else:
        session['is_admin'] = False
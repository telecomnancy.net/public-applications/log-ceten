from flask import Blueprint, session, redirect, url_for, flash, render_template, request
from flask_dance.contrib.google import google
from routes.auth import admin_required
from utils.db import *
admin_panel = Blueprint('admin_panel', __name__)


@admin_panel.route("/panel")
@admin_required
def panel():
    return render_template("admin.html")

"""
def get_pending_requests():
    with admin_panel.app_context():
        db = get_db()
"""

@admin_panel.route("/emprunts")
@admin_required
def emprunts(action=None, loan_id=None):
    action = request.args.get('action')
    loan_id = request.args.get('loan_id')
    if action and loan_id:
        if action == 'accept':
            accept_loan(loan_id)
            redirect(url_for('admin_panel.emprunts'))
        if action == 'reject':
            delete_loan(loan_id)
            redirect(url_for('admin_panel.emprunts'))
        if action == 'return':
            delete_loan(loan_id)
            redirect(url_for('admin_panel.emprunts'))


    ongoing = get_ongoing_loans()
    late = get_late_loans()
    pending = get_pending_loans()
    items = get_items_dict()
    places = get_places()
    return render_template("admin_emprunts.html", ongoing=ongoing, late=late, pending=pending, items=items, places=places)


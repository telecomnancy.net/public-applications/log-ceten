document.addEventListener('DOMContentLoaded', function () {
    var startDateInput = document.getElementById('start_date_input');
    var endDateInput = document.getElementById('end_date_input');
    var durationOutput = document.getElementById('duration_output');

    startDateInput.addEventListener('change', updateDuration);
    endDateInput.addEventListener('change', updateDuration);

    function updateDuration() {
        var startDate = new Date(startDateInput.value);
        var endDate = new Date(endDateInput.value);

        if (!isNaN(startDate.getTime()) && !isNaN(endDate.getTime())) {
            var duration = endDate - startDate;

            // Calculate days, hours, minutes, and seconds
            var days = Math.floor(duration / (24 * 60 * 60 * 1000));
            var hours = Math.floor((duration % (24 * 60 * 60 * 1000)) / (60 * 60 * 1000));
            var minutes = Math.floor((duration % (60 * 60 * 1000)) / (60 * 1000));
            var seconds = Math.floor((duration % (60 * 1000)) / 1000);

            // Display the duration
            durationOutput.textContent = `Durée de l'emprunt: ${days}j, ${hours}h, ${minutes} min`;
        } else {
            // Clear the duration if either date is not set
            durationOutput.textContent = '';
        }
    }
});

document.addEventListener('DOMContentLoaded', function () {
    var userTypeSelect = document.getElementById('item_input');
    var customTypeInput = document.getElementById('custom_item_input');

    userTypeSelect.addEventListener('change', function () {
        // Show the text field only if the selected option is 'custom'
        customTypeInput.style.display = (userTypeSelect.value === 'Nouvel Item') ? 'block' : 'none';
    });
});
